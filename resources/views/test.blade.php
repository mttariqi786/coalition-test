@extends('layouts.master')

@section('contents')

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form id="addProducts" action="{{route('store')}}" method="POST">
                    @csrf
                    <div class="form-group" id="product_name">
                        <label for="product_name">Product Name</label>
                        <input type="text" class="form-control"
                               placeholder="Product Name" name="product_name"
                               value="{{old('product_name')}}">
                    </div>
                    <div class="form-group" id="quantity_in_stock">
                        <label for="quantity_in_stock">Quantity In Stock</label>
                        <input type="number"  class="form-control"
                               placeholder="Quantity In Stock" name="quantity_in_stock"
                               value="{{old('quantity_in_stock')}}">
                    </div>
                    <div class="form-group" id="price_per_item">
                        <label for="price_per_item">Price Per Item</label>
                        <input type="number"  class="form-control"
                               placeholder="Price Per Item" name="price_per_item"
                               value="{{old('price_per_item')}}">
                    </div>

                    <button class="btn btn-info" type="submit">Submit</button>
                </form>
            </div>


            <hr>


            <div class="col-md-6 offset-md-3 mt-5">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Quantity In Stock</th>
                            <th>Price Per Item</th>
                            <th>Datetime Submitted</th>
                            <th>Total value number.</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $product_data)
                            <tr>
                                <td>{{$product_data['product_name']}}</td>
                                <td>{{$product_data['quantity_in_stock']}}</td>
                                <td>{{$product_data['price_per_item']}}</td>
                                <td>{{$product_data['datetime_submitted']}}</td>
                                <td>{{$product_data['quantity_in_stock'] * $product_data['price_per_item']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>





        </div>

    </div>

    <script>

    $(document).ready(function () {


        $('#addProducts').on('submit', function (e) {

            $.ajax({
                type: "POST",
                url: "{{route('store')}}",

                success: function (response) {

                    swal.fire(
                        'Success',
                        'User has been Activated Successfully!',
                        'success'
                    );
                },
                error: function (error) {
                    console.log(error)
                    swal.fire(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                    )

                },
            });

        });
    }


@endsection